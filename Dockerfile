FROM node:carbon

# create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# copy application source
COPY . /usr/src/app

# install dependencies
RUN npm i

# packages/common
WORKDIR /usr/src/app/packages/common
RUN npm i

# packages/theme
WORKDIR /usr/src/app/packages/theme
RUN npm i

# packages/registrar
WORKDIR /usr/src/app/packages/registrar
RUN npm i

# build application
WORKDIR /usr/src/app
RUN npm run build

#set environment variables
ENV IP=0.0.0.0
ENV PORT=6200

EXPOSE 6200
CMD [ "npm", "run", "prod" ]

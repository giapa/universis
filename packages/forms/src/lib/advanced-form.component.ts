import { Component, Input, OnInit, OnChanges, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { FormioComponent } from 'angular-formio/components/formio/formio.component';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedFormsService } from './advanced-forms.service';
import {ErrorService} from '@universis/common';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'advanced-form-edit',
  // tslint:disable-next-line: max-line-length
  template: `<formio #form [form]="formConfig" (formLoad)='onLoad($event)' [refresh]="refreshForm"></formio>`,
  providers: [AdvancedFormsService]
})

export class AdvancedFormComponent implements OnInit, OnChanges {
  @Input('data') data: any;
  @Input('formName') formName: string;
  @ViewChild('form') form: FormioComponent;
  @Output() refreshForm = new EventEmitter<any>();
  @Input('formConfig') formConfig;

  @Output() formSubmitted = new EventEmitter<any>();

  constructor(private _context: AngularDataContext,
    private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _formService: AdvancedFormsService,
    private _errorService: ErrorService) {
  }

  onLoad(event) {
    if (event && Object.keys(event).length === 0) {
      return;
    }
    const currentLang = this._translateService.currentLang;
    const translation = this._translateService.instant('Forms');
    if (this.formConfig.settings && this.formConfig.settings.i18n) {
      // try to get local translations
      if (Object.prototype.hasOwnProperty.call(this.formConfig.settings.i18n, currentLang)) {
        // assign translations
        Object.assign(translation, this.formConfig.settings.i18n[currentLang]);
      }
    }
    this.form.formio.i18next.options.resources[currentLang] = { translation : translation };
    this.form.formio.language = currentLang;
    this.refreshForm.emit({
      submission: {
        data: this.data
      }
    });

    if (this.form.formio.wizard) {
      // NOTE: Should be replaced by builder options
      this.form.formio.options.buttonSettings.showNext = false;
      this.form.formio.options.buttonSettings.showPrevious = false;
      this.form.formio.options.buttonSettings.showCancel = false;
      this.form.formio.options.buttonSettings.showSubmit = false;
      this.form.formio.options.breadcrumbSettings.clickable = true;

      // FIXME: These run twice at some point, but it may be fixed
      this.form.formio.on('toNextStep', () => {
        this.form.formio.nextPage();
      });

      this.form.formio.on('toPreviousStep', () => {
        this.form.formio.prevPage();
      });

    }
  }

  async ngOnInit() {

    this._formService.loadForm(this.formName).then( result => {
      this.formConfig = result;
    });

    this.form.submitExecute = (submission: any) => {
      (async (submissionData: any) => {
        try {
          submissionData.data = JSON.parse(JSON.stringify(submissionData.data), (key, value) => {
            if (value === '' ) {
              value =  null;
            }
            return value;
          });
          return await this._context.getService().execute({
            method: 'POST',
            url: this.formConfig.model,
            headers: {},
            data: submissionData.data
          });
        } catch (error) {
          throw error;
        }
      })(submission).then((result) => {
        this.data = result;
        this.form.onSubmit(submission, true);
        //  Prepare message and notify advance-form-router about form submission
        let toastMessage;
        if ( this.formConfig.submitMessage && this.formConfig.submitMessage[this._translateService.currentLang] ) {
          toastMessage = {
            title: this.formConfig.submitMessage[this._translateService.currentLang].title,
            body: this.formConfig.submitMessage[this._translateService.currentLang].body
          };
        } else {
          toastMessage = {
            title: this._translateService.instant('Forms.FormIOdefaultSuccessfulSubmitTitle'),
            body: this._translateService.instant('Forms.FormIOdefaultSuccessfulSubmitMessage')
          };
        }
        const valueEmitted = { toastMessage };
        this.formSubmitted.emit(valueEmitted);
      }).catch( err => {
        if (err.status) {
          const translatedError = this._translateService.instant(`E${err.status}`);
          this._errorService.showError(translatedError.message, {
            continueLink: '.'
          });
          return this.form.onError(new Error(translatedError.message));
        }
        this._errorService.showError(err, {
          continueLink: '.'
        });
        this.form.onError(err);
      });
    };
  }

  ngOnChanges(changes?: any) {
    if(changes.data && changes.data.currentValue){
      this.data = changes.data.currentValue;
      if(!changes.data.previousValue || changes.data.currentValue !== changes.data.previousValue){
        this.refreshForm.emit({submission: {data: changes.data.currentValue}});
      }
    }
  }
}

const path = require('path');
const rimraf = require('rimraf');
const copy = require('copy-concurrently');
// get source directory ./assets
const src = path.resolve(__dirname, 'src/assets');
// get destination directory ../../dist/forms/assets
const dest = path.resolve(__dirname, '../../dist/forms/assets');
// remove destination files
rimraf.sync(dest);
console.log('Copy Assets');
console.log('From: ' + src);
console.log('To: ' + dest);
// copy assets
copy(src, dest).then(() => {
    // copy assets completed
    return process.exit(0);
}).catch(err => {
    console.log('ERROR', err);
    // exit with error
    return process.exit(-2);
});

import {Injectable} from '@angular/core';
import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class InternshipsTableConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./internships-table.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./internships-table.config.list.json`);
      });
  }
}

export class InternshipsTableSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./internships-table.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./internships-table.search.list.json`);
      });
  }
}

export class InternshipsDefaultTableConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./internships-table.config.list.json`);
  }
}

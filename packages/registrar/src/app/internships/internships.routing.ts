import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InternshipsHomeComponent } from './components/internships-home/internships-home.component';
import { InternshipsTableComponent } from './components/internships-table/internships-table.component';
import { InternshipsPreviewComponent } from './components/internships-preview/internships-preview.component';
import { InternshipsRootComponent } from './components/internships-root/internships-root.component';
import { InternshipsPreviewGeneralComponent } from './components/internships-preview-general/internships-preview-general.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
    ActiveDepartmentResolver,
    CurrentAcademicPeriodResolver,
    CurrentAcademicYearResolver, LastStudyProgramResolver
  } from '../registrar-shared/services/activeDepartmentService.service';
import {
  InternshipsTableConfigurationResolver,
  InternshipsTableSearchResolver
} from './components/internships-table/internships-table-config.resolver';

const routes: Routes = [
    {
        path: '',
        component: InternshipsHomeComponent,
        data: {
            title: 'Internships'
        },
        children: [
            {
              path: '',
              pathMatch: 'full',
              redirectTo: 'list/active'
            },
            {
              path: 'list',
              pathMatch: 'full',
              redirectTo: 'list/index'
            },
            {
              path: 'list/:list',
              component: InternshipsTableComponent,
              data: {
                  title: 'Internships'
              },
              resolve: {
                tableConfiguration: InternshipsTableConfigurationResolver,
                searchConfiguration: InternshipsTableSearchResolver
              }
            }
        ]
    },
    {
        path: ':id',
        component: InternshipsRootComponent,
        data: {
            title: 'Internship Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: InternshipsPreviewComponent,
                data: {
                    title: 'Internship Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: InternshipsPreviewGeneralComponent,
                        data: {
                            title: 'Internships Preview General'
                        }
                    }
                ]
            },
            {
                path: ':action',
                component: AdvancedFormRouterComponent
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class InternshipsRoutingModule {
}

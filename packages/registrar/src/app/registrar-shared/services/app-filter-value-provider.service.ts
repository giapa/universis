import {Injectable, OnDestroy} from '@angular/core';
import {ActiveDepartmentService} from './activeDepartmentService.service';
import {Subscription} from 'rxjs';
import {AdvancedFilterValueProvider} from '../../tables/components/advanced-table/advanced-filter-value-provider.service';

@Injectable({
  providedIn: 'root'
})
export class AppFilterValueProvider extends  AdvancedFilterValueProvider implements OnDestroy {

  public values = {
    currentDepartment: null,
    currentYear: null,
    currentPeriod: null
  };

  private readonly _activeDepartmentSource: Subscription;

  constructor(private _activeDepartmentService: ActiveDepartmentService) {
    super();
    //
    this._activeDepartmentSource = this._activeDepartmentService.departmentChange.subscribe( value => {
      Object.assign(this.values, {
        currentDepartment: value && value.id,
        currentYear: value && value.currentYear && value.currentYear.id,
        currentPeriod: value && value.currentPeriod && value.currentPeriod.id
      });
    });

  }

  ngOnDestroy(): void {
    if (this._activeDepartmentSource) {
      this._activeDepartmentSource.unsubscribe();
    }
  }

  async getValues(): Promise<any> {
    const value = await this._activeDepartmentService.getActiveDepartment();
    Object.assign(this.values, {
      currentDepartment: value && value.id,
      currentYear: value && value.currentYear && value.currentYear.id,
      currentPeriod: value && value.currentPeriod && value.currentPeriod.id
    });
    return this.values;
  }

}

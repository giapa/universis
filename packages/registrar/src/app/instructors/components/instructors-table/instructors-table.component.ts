import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as INSTRUCTORS_LIST_CONFIG from './instructors-table.config.list.json';
import { AdvancedTableComponent, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivatedTableService } from '../../../tables/tables.activated-table.service';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import { Subscription } from 'rxjs';
import { UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';

@Component({
  selector: 'app-instructors-table',
  templateUrl: './instructors-table.component.html'
})
export class InstructorsTableComponent implements OnInit, OnDestroy {

  public readonly config = INSTRUCTORS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Instructors.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}

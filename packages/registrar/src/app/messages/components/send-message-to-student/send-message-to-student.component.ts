import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ErrorService, LoadingService, ModalService, DIALOG_BUTTONS, ToastService,  } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { MessagesService } from '../../services/messages.service';


@Component({
  selector: 'app-send-message-to-student',
  templateUrl: './send-message-to-student.component.html'
})
export class SendMessageToStudentComponent implements OnInit {

  @Input() studentId: number;
  @Input() showMessageForm = false;   //  A value passed from parent component to define if the message send form should be displayed
  public messageModel = {             //  A structure that describes a message properties
    body: null,
    attachment: null,
    subject: null
  };

  @Output() succesfulSend = new EventEmitter<boolean>();

  @ViewChild('fileinput') fileinput;

  constructor(private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _messagesService: MessagesService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translate: TranslateService) { }

  ngOnInit() {
  }

/*
 *  A function to initialize / reset our message data
 */
  initData() {
//  initialize values
    this.messageModel.attachment = null;
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }

/*
 *  A function to sends a message.
 */
  save() {
    this._modalService.showDialog( this._translate.instant('Modals.ModalSendMessageTitle'),
                                   this._translate.instant('Modals.ModalSendMessageMessage'),
                                   DIALOG_BUTTONS.OkCancel)
      .then(res => {
        if (res === 'ok') {
          this._loadingService.showLoading();
          this._messagesService.sendMessageToStudent(this.studentId, this.messageModel)
            .subscribe(res => {
              // complete the action
              this.initData();

              this._loadingService.hideLoading();
              this._toastService.show( this._translate.instant('Modals.SuccessfulSendMessageTitle'),
                                       this._translate.instant('Modals.SuccessfulSendMessageMessage'));
              let container = document.body.getElementsByClassName('universis-toast-container')[0];
              // Removed this. If added Toast message text appears in toast-success color (greenish)
              // if (container != null) {
              //   container.classList.add('toast-success');
              // }
              this.messageSent(true);
          }, (err) => {
            this._loadingService.hideLoading();
            this._toastService.show( this._translate.instant('Modals.FailedSendMessageTitle'),
                                     this._translate.instant('Modals.FailedSendMessageMessage'));
            let container = document.body.getElementsByClassName('universis-toast-container')[0];
            if (container != null) {
              container.classList.add('toast-error');
            }
            this.messageSent(false);
          });
        }
      });
  }

/*
 *  A function to get the selected file
 */
  onFileChanged(event) {
    this.messageModel.attachment = this.fileinput.nativeElement.files[0];
  }

/*
 *  A function to reset the file of html input element
 */
  reset() {
    this.fileinput.nativeElement.value = '';
    this.messageModel.attachment = null;
  }

/*
 *  A function to emit status of message sent to parent component
 */
  messageSent(succesful: boolean) {
    this.succesfulSend.emit(succesful);
  }
  close() {
    this.showMessageForm = !this.showMessageForm;
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentAcademicYearResolver, CurrentAcademicPeriodResolver, ActiveDepartmentIDResolver } from '../registrar-shared/services/activeDepartmentService.service';
import { GraduationsHomeComponent } from './components/graduations-home/graduations-home.component';
import { GraduationsTableComponent } from './components/graduations-table/graduations-table.component';
import {
  GraduationsTableConfigurationResolver,
  GraduationsTableSearchResolver
} from './components/graduations-table/graduations-table-config.resolver';
import { GraduationsRootComponent } from './components/graduations-root/graduations-root.component';
import { AdvancedFormRouterComponent } from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {GraduationEditComponent} from './components/graduation-edit/graduation-edit.component';
import {GraduationsDashboardComponent} from './components/graduations-dashboard/graduations-dashboard.component';
import {GraduationsOverviewComponent} from './components/graduations-dashboard/graduations-overview/graduations-overview.component';
import {GraduationsStudentRequestsComponent} from './components/graduations-dashboard/graduations-student-requests/graduations-student-requests.component';
import {GraduationsGeneralComponent} from './components/graduations-dashboard/graduations-general/graduations-general.component';
import {GraduationsGraduatesComponent} from './components/graduations-dashboard/graduations-graduates/graduations-graduates.component';
import {GraduationsRequestsRootComponent} from './components/graduations-dashboard/graduations-student-requests/graduations-requests-root/graduations-requests-root.component';

const routes: Routes = [
  {
    path: '',
    component: GraduationsHomeComponent,
    data: {
      title: 'GraduationEvents'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/opened'
      },
      {
        path: 'list/:list',
        component: GraduationsTableComponent,
        data: {
          title: 'Graduations List'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: GraduationsTableConfigurationResolver,
          searchConfiguration: GraduationsTableSearchResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: GraduationEditComponent,
            outlet: 'modal',
            data: {
              action: 'new',
              model: 'GraduationEvents'
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: GraduationEditComponent,
            outlet: 'modal',
            data: {
              action: 'edit',
              model: 'GraduationEvents'
            }
          }
        ]
      }
    ]
  },
  {
    path: 'create',
    component: GraduationsHomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        resolve: {
          organizer: ActiveDepartmentIDResolver,
          graduationYear: CurrentAcademicYearResolver,
          graduationPeriod: CurrentAcademicPeriodResolver
        }
      }
    ]
  },
  {
    path: ':id',
    component: GraduationsRootComponent,
    data: {
      title: 'Graduation Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: GraduationsDashboardComponent,
        data: {
          title: 'Graduations.Dashboard'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: GraduationsOverviewComponent,
            data: {
              title: 'Graduations.Overview'
            }
          },
          {
            path: 'general',
            component: GraduationsGeneralComponent,
            data: {
              title: 'Graduations.General'
            }
          },
          {
            path: 'requests',
            component: GraduationsStudentRequestsComponent,
            data: {
              title: 'Graduations.Requests'
            }
          },
          {
            path: 'graduates',
            component: GraduationsGraduatesComponent,
            data: {
              title: 'Graduations.Graduates',
            },
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class GraduationsRoutingModule {
}

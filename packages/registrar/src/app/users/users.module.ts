import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRootComponent } from './components/users-root/users-root.component';
import { UsersHomeComponent } from './components/users-home/users-home.component';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UsersSharedModule } from './users.shared';
import { UsersRoutingModule } from './users.routing';
import { TablesModule } from '../tables/tables.module';
import { TranslateModule } from '@ngx-translate/core';
import {SharedModule} from '@universis/common';
import {MostModule} from '@themost/angular';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';
import { UsersDashboardComponent } from './components/users-dashboard/users-dashboard.component';
import { UsersDashboardOverviewComponent } from './components/users-dashboard/users-dashboard-overview/users-dashboard-overview.component';
import {RouterModalModule} from '@universis/common/routing';
import { UsersDashboardOverviewGeneralComponent } from './components/users-dashboard/users-dashboard-overview/users-dashboard-overview-general/users-dashboard-overview-general.component';
import { UsersDashboardOverviewGroupsComponent } from './components/users-dashboard/users-dashboard-overview/users-dashboard-overview-groups/users-dashboard-overview-groups.component';
import { UsersDashboardOverviewDepartmentsComponent } from './components/users-dashboard/users-dashboard-overview/users-dashboard-overview-departments/users-dashboard-overview-departments.component';
import {AdvancedFormsModule} from '@universis/forms';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    UsersRoutingModule,
    UsersSharedModule,
    SharedModule,
    MostModule,
    FormsModule,
    ElementsModule,
    RouterModalModule,
    RegistrarSharedModule,
    AdvancedFormsModule
  ],
  declarations: [UsersRootComponent,
    UsersHomeComponent,
    UsersTableComponent,
    UsersDashboardComponent,
    UsersDashboardOverviewComponent,
    UsersDashboardOverviewGeneralComponent,
    UsersDashboardOverviewGroupsComponent,
    UsersDashboardOverviewDepartmentsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UsersModule { }

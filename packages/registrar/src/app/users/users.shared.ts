import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateService, TranslateModule} from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import {UserDefaultTableConfigurationResolver,
  UserTableConfigurationResolver,
  UserTableSearchResolver} from './components/users-table/users-table-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule
  ],
  declarations: [
  ],
  exports : [
  ],
  providers: [
    UserTableConfigurationResolver,
    UserTableSearchResolver,
    UserDefaultTableConfigurationResolver
  ]
})
export class UsersSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading scholarships shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/users.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}

import {Injectable, ModuleWithProviders, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule,TranslateService} from '@ngx-translate/core';
import {ConfigurationService, SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {StudyProgramsPreviewFormComponent} from './components/preview/preview-general/study-programs-preview-form.component';
import { ProgramCoursePreviewFormComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-form.component';
import { StudyProgramsTableConfigurationResolver, StudyProgramsTableSearchResolver, StudyProgramsDefaultTableConfigurationResolver } from './components/study-programs-table/study-programs-table-config.resolver';
import { StudyProgramsPreviewCoursesConfigurationResolver, StudyProgramsPreviewCoursesSearchResolver, StudyProgramsDefaultPreviewCoursesConfigurationResolver } from './components/preview/preview-courses/study-programs-preview-courses-config.resolver';
import { EditCoursesComponent } from './components/preview/edit-courses/edit-courses.component';
import {AngularDataContext, MostModule} from '@themost/angular';
import {RouterModalModule} from '@universis/common/routing';
import {AdvancedFormItemResolver, AdvancedFormsModule} from '@universis/forms';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {CoursesSharedModule} from '../courses/courses.shared';
import {AddCoursesComponent} from './components/preview/add-courses/add-courses.component';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModalModule,
    AdvancedFormsModule,
    SettingsSharedModule,
      CoursesSharedModule
  ],
  declarations: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent,
    EditCoursesComponent,
    AddCoursesComponent
  ],
  entryComponents: [
    EditCoursesComponent,
    AddCoursesComponent
  ],
  exports: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent,
    EditCoursesComponent
  ],
  providers: [
  ]
})
export class StudyProgramsSharedModule implements OnInit {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StudyProgramsSharedModule,
      providers: [
        StudyProgramsTableConfigurationResolver,
        StudyProgramsTableSearchResolver,
        StudyProgramsDefaultTableConfigurationResolver,
        StudyProgramsPreviewCoursesConfigurationResolver,
        StudyProgramsPreviewCoursesSearchResolver,
        StudyProgramsDefaultPreviewCoursesConfigurationResolver
      ]
    };
  }

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading study programs shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/study-programs.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}
@Injectable({ providedIn: 'root' })
export class StudyProgramItemWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (item.info && item.info.locales) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.info.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.info.locales.splice(removeIndex, 1);
        }

        // map application locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if(locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.info.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.info.locales.push({
                inLanguage: locale
              });
            }
          }
        }
      }
      return Promise.resolve(item);
    });
  }
}


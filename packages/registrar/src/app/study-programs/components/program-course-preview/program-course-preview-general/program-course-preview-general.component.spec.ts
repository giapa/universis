import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramCoursePreviewGeneralComponent } from './program-course-preview-general.component';

describe('ProgramCoursePreviewGeneralComponent', () => {
  let component: ProgramCoursePreviewGeneralComponent;
  let fixture: ComponentFixture<ProgramCoursePreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramCoursePreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramCoursePreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

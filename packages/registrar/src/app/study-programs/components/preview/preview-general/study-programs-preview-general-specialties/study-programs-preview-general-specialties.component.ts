import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-study-programs-preview-general-specialties',
  templateUrl: './study-programs-preview-general-specialties.component.html',
  styleUrls: ['./study-programs-preview-general-specialties.component.scss']
})
export class StudyProgramsPreviewGeneralSpecialtiesComponent implements OnInit {

  public specialties: any;
  public studyProgramID: any;

  constructor( private _activatedRoute: ActivatedRoute,
               private _context: AngularDataContext) { }

  async ngOnInit() {

    this.studyProgramID = this._activatedRoute.snapshot.params.id;
    this.specialties = await this._context.model('StudyProgramSpecialties')
    .where('studyProgram').equal(this._activatedRoute.snapshot.params.id)
    .prepare()
    .take(-1)
    .getItems();

  }

}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StudyProgramsHomeComponent} from './components/study-programs-home/study-programs-home.component';
import {StudyProgramsTableComponent} from './components/study-programs-table/study-programs-table.component';
import {StudyProgramsRootComponent} from './components/study-programs-root/study-programs-root.component';
import {StudyProgramsPreviewComponent} from './components/preview/study-programs-preview.component';
import {StudyProgramsPreviewGeneralComponent} from './components/preview/preview-general/study-programs-preview-general.component';
import {StudyProgramsPreviewCoursesComponent} from './components/preview/preview-courses/study-programs-preview-courses.component';
import { ProgramCoursePreviewComponent } from './components/program-course-preview/program-course-preview.component';
import { ProgramCoursePreviewGeneralComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-general.component';
import {
    AdvancedFormItemResolver,
    AdvancedFormModalComponent,
    AdvancedFormModalData
} from '@universis/forms';
import {
  AdvancedFormRouterComponent
} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import { StudyProgramsTableConfigurationResolver, StudyProgramsTableSearchResolver } from './components/study-programs-table/study-programs-table-config.resolver';
import { StudyProgramsPreviewCoursesConfigurationResolver, StudyProgramsPreviewCoursesSearchResolver } from './components/preview/preview-courses/study-programs-preview-courses-config.resolver';
import { ActiveDepartmentResolver} from '../registrar-shared/services/activeDepartmentService.service';
import {StudyProgramItemWithLocalesResolver} from './study-programs.shared';

const routes: Routes = [
    {
        path: '',
        component: StudyProgramsHomeComponent,
        data: {
            title: 'Study Programs'
        },
        children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'list/active'
        },
        {
          path: 'list',
          pathMatch: 'full',
          redirectTo: 'list/active'
        },
        {
          path: 'list/:list',
          component: StudyProgramsTableComponent,
          data: {
            title: 'Study Programs List'
          },
          resolve: {
            tableConfiguration: StudyProgramsTableConfigurationResolver,
            searchConfiguration: StudyProgramsTableSearchResolver
          }
        }
      ]
    },
  {
    path: 'create',
    component: StudyProgramsRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        data: {
          '$state': 1
        },
      }
    ],
    resolve: {
      department: ActiveDepartmentResolver
    },
  },
    {
     path: ':id',
        component: StudyProgramsRootComponent,
        data: {
            title: 'Study Programs Home'
        },
        children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'preview'
        },
        {
          path: 'preview',
          component: StudyProgramsPreviewComponent,
          data: {
            title: 'Study Programs Preview'
          },
          children: [
            {
                path: '',
                redirectTo: 'general'
            },
            {
                path: 'general',
                component: StudyProgramsPreviewGeneralComponent,
                data: {
                    title: 'Study Programs Preview General'
                }
            },
              {
                  path: 'specialization/:specialization/courses',
                  component: StudyProgramsPreviewCoursesComponent,
                  data: {
                      title: 'Study Programs Preview Courses'
                  },
                  resolve: {
                      tableConfiguration: StudyProgramsPreviewCoursesConfigurationResolver,
                      searchConfiguration: StudyProgramsPreviewCoursesSearchResolver,
                      department: ActiveDepartmentResolver
                  },
                  children: [
                      {
                          path: 'item/:id/edit',
                          pathMatch: 'full',
                          component: AdvancedFormModalComponent,
                          outlet: 'modal',
                          data: <AdvancedFormModalData> {
                              model: 'SpecializationCourses',
                              action: 'edit',
                              closeOnSubmit: true,
                              serviceQueryParams: {
                                  $expand: 'studyProgramCourse($expand=course),specialization',
                              }
                          },
                          resolve: {
                              data: AdvancedFormItemResolver
                          }
                      }
                  ]
              },
            {
                path: 'courses',
                component: StudyProgramsPreviewCoursesComponent,
                data: {
                    title: 'Study Programs Preview Courses'
                },
                resolve: {
                  tableConfiguration: StudyProgramsPreviewCoursesConfigurationResolver,
                  searchConfiguration: StudyProgramsPreviewCoursesSearchResolver,
                  department: ActiveDepartmentResolver
                },
                children: [
                    {
                        path: 'programCourses/:id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData> {
                            model: 'SpecializationCourses',
                            action: 'edit',
                            serviceQueryParams: {
                                $expand: 'studyProgramCourse($expand=course),specialization',
                            }
                        },
                        resolve: {
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            }
        ]

        },
          {
            path: 'edit',
            component: AdvancedFormRouterComponent,
            data: {
              title: 'StudyPrograms.Edit',
              action: 'edit',
              serviceQueryParams: {
                $expand: 'department, studyLevel, gradeScale, info($expand=studyTitleType,specializationTitleType,locales)'
              }
            },
            resolve: {
              data: StudyProgramItemWithLocalesResolver
            }
          },
        {
          path: ':action',
          component: AdvancedFormRouterComponent,
        }
      ]
    },
    {
      path: ':ProgramID/courses/:CourseID',
      component: ProgramCoursePreviewComponent,
      data: {
        title: 'Study Programs Preview'
      },
      children: [
        {
            path: '',
            redirectTo: 'general'
        },
        {
            path: 'general',
            component: ProgramCoursePreviewGeneralComponent,
            data: {
                title: 'Study Programs Preview General'
            }
        }
      ]

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class StudyProgramsRoutingModule {
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewGeneralExamInfoComponent } from './exams-preview-general-exam-info.component';

describe('ExamsPreviewGeneralExamInfoComponent', () => {
  let component: ExamsPreviewGeneralExamInfoComponent;
  let fixture: ComponentFixture<ExamsPreviewGeneralExamInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewGeneralExamInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewGeneralExamInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseExamUploadActionsComponent } from './course-exam-upload-actions.component';

describe('CourseExamUploadActionsComponent', () => {
  let component: CourseExamUploadActionsComponent;
  let fixture: ComponentFixture<CourseExamUploadActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseExamUploadActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseExamUploadActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

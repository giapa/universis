import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class ExamsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./exams-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./exams-table.config.list.json`);
        });
    }
}

export class ExamsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./exams-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./exams-table.search.list.json`);
            });
    }
}

export class ExamsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./exams-table.config.list.json`);
    }
}

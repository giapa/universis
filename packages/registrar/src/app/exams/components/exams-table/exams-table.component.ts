import {Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter} from '@angular/core';
import * as EXAMS_LIST_CONFIG from './exams-table.config.json';
import { AdvancedTableComponent, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component.js';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';
import {TranslateService} from '@ngx-translate/core';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {AdvancedRowActionComponent} from '../../../tables/components/advanced-row-action/advanced-row-action.component';
import {ClientDataQueryable} from '@themost/client';

@Component({
  selector: 'app-exams-table',
  templateUrl: './exams-table.component.html',
})
export class ExamsTableComponent implements OnInit, OnDestroy {

  public readonly config = EXAMS_LIST_CONFIG;
  private dataSubscription: Subscription;
  public recordsTotal: any;
  private selectedItems: any;
  private takeSize = 100;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _activatedTable: ActivatedTableService) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  onSelectBoxChange(event: any) {
    // get value
    const value = parseInt((<HTMLSelectElement>event.target).value, 10);
    if (isNaN(value)) {
      this._router.navigate(
        [],
        {
          relativeTo: this._activatedRoute,
          queryParams: { $filter: '' },
          queryParamsHandling: 'merge'
        }).then(() => {
          this.table.fetch();
        });
      return;
    }
    this._router.navigate(
      [],
      {
        relativeTo: this._activatedRoute,
        queryParams: { $filter: `status eq ${value}` },
        queryParamsHandling: 'merge'
      }).then(() => {
        this.table.fetch();
      });
  }


  /**
   * Executes open action for course classes
   */
  /**
   * Executes open action for course classes
   */
  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id,
          status: {
            alternateName: status
          }
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('CourseExams').save(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  async openAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.status === 'closed';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.OpenAction.Title',
          description: 'Exams.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('open')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async closeAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.status === 'open';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.CloseAction.Title',
          description: 'Exams.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Deletes selected course exams
   */
  async deleteAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items without students
      this.selectedItems = items.filter( (item) => {
        return (!item.numberOfGradedStudents || item.numberOfGradedStudents === 0) && item.status !== 'completed';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.DeleteAction.Title',
          description: 'Exams.DeleteAction.Description',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes copy action for course classes
   */
  executeDeleteAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('CourseExams').remove(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'status/alternateName as status', 'year/id as year', 'examPeriod', 'numberOfGradedStudents'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.status,
              year: item.year.id,
              examPeriod: item.examPeriod,
              numberOfGradedStudents: item.numberOfGradedStudents
            };
          });
        }
      }
    }
    return items;
  }
}

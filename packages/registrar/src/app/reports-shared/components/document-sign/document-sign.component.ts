import {Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation, Output, EventEmitter} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import 'rxjs/add/observable/combineLatest';
import {ErrorService, LoadingService, ModalService, ToastService, UserService} from '@universis/common';
import {RouterModalOkCancel} from '@universis/common/routing';
import {FormioComponent, FormioRefreshValue} from 'angular-formio';
import {SignerService} from '../../services/signer.service';
import {Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AppEventService} from '@universis/common';
import {base64StringToBlob, blobToBase64String} from 'blob-util';

declare var $: any;

@Component({
  selector: 'app-document-sign',
  templateUrl: './document-sign.component.html',
  styles: [
      `

      @keyframes FadeIn {
        from {
          color: #664BF1;
        }

        to {
          background-color: #FFF;
        }
      }

      .loading-icon {
        color: white;
        animation: FadeIn 3s ease-in-out forwards infinite;
      }
    `
  ]
})
export class DocumentSignComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @ViewChild('form') form: FormioComponent;
  @Output() refreshForm: EventEmitter<FormioRefreshValue> = new EventEmitter<FormioRefreshValue>();

  private statusSubscription: Subscription;
  @Input() item: any;
  public signForm: any;
  public signFormData: any = {};
  public lastError: any;
  public loading = false;
  public signatures: File[] = [];

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              protected _translateService: TranslateService,
              protected _context: AngularDataContext,
              protected _errorService: ErrorService,
              protected _signer: SignerService,
              protected _modalService: ModalService,
              protected _http: HttpClient,
              protected _appEvent: AppEventService,
              protected _toastService: ToastService,
              protected _loadingService: LoadingService,
              protected _userService: UserService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Reports.Sign';
    // disable ok button
    this.okButtonDisabled = true;
    // set button text
    this.okButtonText = 'OK';
    // load signature graphic
    this.tryToGetSignatureGraphic();
  }

  async ngOnInit() {

    // get sign form
    let signForm;
    if (this.item != null) {
      signForm = await this._signer.getSignFormFor(this.item);
    }
    // set report form with no data
    this.signForm = signForm;
    this.statusSubscription = this._signer.queryStatus().subscribe( (serviceStatus) => {
      if (serviceStatus != null) {
        // set service status
        const formData = Object.assign({ }, this.item, {
          signerServiceStatus: serviceStatus.status,
          published: false,
          lastError: 0
        });
        // if signer service is already authenticated
        if (this._signer.authorization) {
          // get certificates
          return this._signer.getCertificates().then( (certs) => {
            // find select certificate component
            const selectComponent = this.findComponent(signForm.components, 'signCertificate');
            // and add certificate values
            if (selectComponent) {
              // map certificate collection
              selectComponent.data.values = certs.map( (cert) => {
                return {
                  label: cert.commonName,
                  value: cert.thumbprint
                };
              });
              const lastCertificate = this._signer.getLastCertificate();
              if (lastCertificate) {
                // find if last certificate exists
                const findCertificate = certs.find( cert => {
                  return cert.thumbprint = lastCertificate;
                });
                if (findCertificate) {
                  Object.assign(formData, {
                    signCertificate: findCertificate.thumbprint
                  });
                }
              } else if (certs.length) {
                Object.assign(formData, {
                  signCertificate: certs[0].thumbprint
                });
              }
            }
            // set signer authentication data
            Object.assign(formData, {
              signerAlreadyAuthenticated: true,
              keepMeSignedInDuringThisSession: true,
              typeKeyStorePassword: '****'
            });
            this.refreshForm.emit({
              form: signForm,
              submission: {
                data: formData
              }
            });
          }).catch( (err) => {
            this.lastError = err;
          });
        }
        // set signer authentication data
        Object.assign(formData, {
          signerAlreadyAuthenticated: false,
          keepMeSignedInDuringThisSession: false,
          typeKeyStorePassword: null
        });
        this.refreshForm.emit({
          form: signForm,
          submission: {
            data: formData
          }
        });
      }
    });

  }

  ngOnDestroy() {
    if (this.statusSubscription) {
      this.statusSubscription.unsubscribe();
    }
    this._signer.destroy();
  }

  cancel(): Promise<any> {
    // close
    if (this._modalService.modalRef) {
      return  this._modalService.modalRef.hide();
    }
  }

  /**
   * Signs current document
   */
  async sign(document) {
    const data = this.form.formio.data;
    // get content location and prepare to sign
    const contentLocation = document.url.replace(/^\/api\//, '/');
    // get document
    const documentURL = this._context.getService().resolve(contentLocation);
    const response = await this._http.get(documentURL, {
      headers: this._context.getService().getHeaders(),
      responseType: 'blob',
      observe: 'response'
    }).toPromise();
    // get blob
    const blob = await this._signer.signDocument(response.body, data.signCertificate, null, this.signatures[0]);
    // replace document
    const updateItem = {
      url: document.url,
      published: data.published,
      datePublished: data.published ? new Date() : null,
      signed: true
    };
    await this._signer.replaceDocument(updateItem, blob);
    return updateItem;
  }

  async ok() {
    try {
      this.loading = true;
      this.lastError = null;
      if (this.item) {
        const updateItem = await this.sign(this.item);
        // and hide
        this.loading = false;
        // close
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
        // emit app change event
        this._appEvent.change.next({
          model: 'DocumentNumberSeriesItems',
          target: Object.assign(this.item, updateItem)
        });
        this._toastService.show(
            this._translateService.instant('Documents.SignedMessage.title'),
            this._translateService.instant('Documents.SignedMessage.message')
        );
      } else {
        this.loading = false;
        // close
        if (this._modalService.modalRef) {
          this._modalService.modalRef.hide();
        }
      }
    } catch (err) {
        this.loading = false;
        this.lastError = err;
    }
  }

  /**
   * Assign localization data while loading form
   */
  onFormLoad() {
    if (this.form == null) {
      return;
    }
    const currentLang = this._translateService.currentLang;
    const translation = this._translateService.instant('Forms');
    if (this.signForm.settings && this.signForm.settings.i18n) {
      // try to get local translations
      if (Object.prototype.hasOwnProperty.call(this.signForm.settings.i18n, currentLang)) {
        // assign translations
        Object.assign(translation, this.signForm.settings.i18n[currentLang]);
      }
    }
    this.form.formio.i18next.options.resources[currentLang] = { translation : translation };
    this.form.formio.language = currentLang;
    this.form.formio.data = this.item;
  }

  /**
   * Validate form and disable OK button
   * @param event
   */
  onChange(event: any) {
    // handle changes (check if event has isValid property)
    if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
      // enable or disable button based on form status
      this.okButtonDisabled = !event.isValid;
    }
    if (event.srcElement && event.srcElement.name === 'data[signCertificate]') {
      if (this.form.formio.submission.data.signCertificate) {
          this._signer.setLastCertificate(this.form.formio.submission.data.signCertificate);
      }
    }
  }

  /**
   * A helper function for finding a component by key name
   */
  findComponent(components: Array<any>, key: any) {
    for (let index = 0; index < components.length; index++) {
      const component = components[index];
      if (component.key === key) {
        return component;
      }
      if (component.columns) {
        for (let j = 0; j < component.columns.length; j++) {
          const column = component.columns[j];
          if (Array.isArray(column.components)) {
            const findComponent = this.findComponent(column.components, key);
            if (findComponent) {
              return findComponent;
            }
          }
        }
      }
      if (component.components) {
        const findComponent = this.findComponent(component.components, key);
        if (findComponent) {
          return findComponent;
        }
      }
    }
  }

  /**
   * Refresh form data
   * @param {*} data
   */
  private refreshFormData(data: any) {
    this.refreshForm.emit({
      submission: {
        data: data
      }
    });
    this.form.ngOnChanges({});
  }

  /**
   * Get certificate list from signer app
   */
  onCustomEvent(event: any) {
    if (event.type === 'certificates') {
      this.lastError = null;
      this._loadingService.showLoading();
      const data = this.form.submission.data;
      const form = this.form.form;
      const authenticateSigner = new Promise<void>((resolve, reject) => {
        if (this._signer.authorization != null) {
          return resolve();
        }
        this._signer.authenticate({
          username: 'user',
          password: data.typeKeyStorePassword,
          rememberMe: data.keepMeSignedInDuringThisSession
        }).then(() => {
          return resolve();
        }).catch((err) => {
          return reject(err);
        });
      });
      authenticateSigner.then(() => {
        return this._signer.getCertificates().then( (certs: Array<any>) => {
          // find component by key
          const selectComponent = this.findComponent(form.components, 'signCertificate');
          if (selectComponent) {
            // map certificate collection
            selectComponent.data.values = certs.map( (cert) => {
              return {
                label: cert.commonName,
                value: cert.thumbprint
              };
            });
            if (selectComponent.data.values.length) {
              data.signCertificate = selectComponent.data.values[0].value;
            }
            // refresh form and data
            this.refreshForm.emit({
              form: form,
              submission: {
                data: data
              }
            });
          }
          this._loadingService.hideLoading();
        });
      }).catch((err) => {
        this.lastError = err;
        this._loadingService.hideLoading();
      });
    }
  }

  onFileSelect(event) {
    this._userService.getUser().then((user) => {
      // get file
      const addedFile = event.addedFiles[0];
      // add file
      this.signatures.push(addedFile);
      // save image to local storage
      blobToBase64String(addedFile).then((value) => {
        localStorage.setItem('user.signatureGraphic', value);
        localStorage.setItem('user.signatureGraphic.type', addedFile.type);
        localStorage.setItem('user.signatureGraphic.name', addedFile.name);
      });
    });
  }

  onFileRemove(event) {
    console.log(event);
    this.signatures.splice(this.signatures.indexOf(event), 1);
    // delete local storage
    localStorage.removeItem('user.signatureGraphic');
    localStorage.removeItem('user.signatureGraphic.type');
    localStorage.removeItem('user.signatureGraphic.name');
  }

  protected tryToGetSignatureGraphic() {
    const value = localStorage.getItem('user.signatureGraphic');
    if (value) {
      const type = localStorage.getItem('user.signatureGraphic.type');
      const name = localStorage.getItem('user.signatureGraphic.name');
      const blob = base64StringToBlob(value, type);
      const addFile = new File([blob], name, {type: type});
      this.signatures.push(addFile);
    }
  }

}

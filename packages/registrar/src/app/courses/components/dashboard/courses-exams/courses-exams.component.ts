import { Component, OnInit, Input, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as COURSES_EXAMS_LIST_CONFIG from './courses-exams.config.list.json';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ErrorService, ModalService, ToastService, DIALOG_BUTTONS } from '@universis/common';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-courses-exams',
  templateUrl: './courses-exams.component.html',
  styleUrls: ['./courses-exams.component.scss']
})

export class CoursesExamsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>COURSES_EXAMS_LIST_CONFIG;

  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private dataSubscription: Subscription;
  private subscription: Subscription;

  @ViewChild('exams') exams: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  // courseID: any = this._activatedRoute.snapshot.params.id;
  public courseId;

  constructor(private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _context: AngularDataContext) { }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.exams;
      this.courseId = params.id;

      this.exams.query = this._context.model('CourseExams')
        .where('course').equal(params.id)
        .prepare();

      this.exams.config = AdvancedTableConfiguration.cast(COURSES_EXAMS_LIST_CONFIG);
       this.exams.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.exams.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.exams.config = data.tableConfiguration;
          this.exams.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.exams && this.exams.selected && this.exams.selected.length) {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.courseId = params.id;
        const items = this.exams.selected.map(item => {
          return {
            course: this.courseId,
            id: item.id
          };
        });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Courses.RemoveExamsTitle'),
        this._translateService.instant('Courses.RemoveExamMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('CourseExams').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Courses.RemoveExamsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Courses.RemoveExamsMessage.one' : 'Courses.RemoveExamsMessage.many')
                  , { value: items.length })
              );
              this.exams.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
      });
    }
  }

}

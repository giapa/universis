import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-study-programs',
  templateUrl: './courses-study-programs.component.html',
  styleUrls: ['./courses-study-programs.component.scss']
})
export class CoursesStudyProgramsComponent implements OnInit, OnDestroy {
  public model: any;
  private subscription: Subscription;


  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('ProgramCourses')
        .where('course').equal(params.id)
        .expand('course,program($expand=department,studyLevel)')
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

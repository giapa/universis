import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentsHomeComponent } from './components/departments-home/departments-home.component';
import { DepartmentsTableComponent } from './components/departments-table/departments-table.component';
import { DepartmentsRootComponent } from './components/departments-root/departments-root.component';
import { DepartmentsPreviewComponent } from './components/departments-preview/departments-preview.component';
import {AdvancedFormItemWithLocalesResolver,AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver, ActiveDepartmentIDResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import { AdvancedListComponent } from '../tables/components/advanced-list/advanced-list.component';
import { AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver } from '@universis/forms';
import * as DepartmentDocumentSeriesConfig from './documents-series.config.list.json';
import * as DepartmentDocumentSeriesEdit from './forms/DepartmentDocumentNumberSeries/edit.json';
import * as DepartmentDocumentSeriesNew from './forms/DepartmentDocumentNumberSeries/new.json';
import {DepartmentsTableConfigurationResolver, DepartmentsTableSearchResolver}
from './components/departments-table/departments-table-config.resolver';
import {DepartmentsDashboardComponent} from './components/departements-dashboard/departments-dashboard.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardOverviewComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardRegistrationsComponent} from './components/departements-dashboard/departments-dashboard-registrations/departments-dashboard-registrations.component';
import {ArchivedDocumentsHomeComponent} from './components/archived-documents-home/archived-documents-home.component';
import { ArchivedDocumentsTableComponent } from './components/archived-documents-table/archived-documents-table.component';
import { ArchivedDocumentsConfigurationResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';
import { ArchivedDocumentsConfigurationSearchResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';

import * as InstituteTableConfiguration from './institutes.config.list.json';
import * as InstituteSearchConfiguration from './institutes.search.list.json';

const routes: Routes = [
    {
        path: '',
        component: DepartmentsHomeComponent,
        data: {
            title: 'Departments'
        },
        children: [
            {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list/index'
          },
          {
            path: 'list',
            pathMatch: 'full',
            redirectTo: 'list/undergraduate'
          },
          {
            path: 'list/:list',
            component: DepartmentsTableComponent,
            data: {
              title: 'Departments List'
            },
            resolve: {
              tableConfiguration: DepartmentsTableConfigurationResolver,
              searchConfiguration: DepartmentsTableSearchResolver
            },

          }
        ]
    },
  {
    path: 'create',
    component: DepartmentsRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        data: {
        },
        resolve: {
          department: ActiveDepartmentResolver,
          inscriptionYear: CurrentAcademicYearResolver,
          inscriptionPeriod: CurrentAcademicPeriodResolver,
          studyProgram: LastStudyProgramResolver
        }
      }
    ]
  },
    {
      path: 'new',
      component: AdvancedFormRouterComponent,
      resolve: {
        department: ActiveDepartmentResolver,
        inscriptionYear: CurrentAcademicYearResolver,
        inscriptionPeriod: CurrentAcademicPeriodResolver,
        studyProgram: LastStudyProgramResolver
      }
    },
    {
      path: 'current/documents',
      component: ArchivedDocumentsHomeComponent,
      resolve: {
        department: ActiveDepartmentResolver
      },
      children: [
        {
          path: 'series/:documentSeries/items',
          component: ArchivedDocumentsTableComponent,
          resolve: {
            documents: ArchivedDocumentsConfigurationResolver,
            searchConfiguration: ArchivedDocumentsConfigurationSearchResolver
          }
        }]
    },
  {
    path: 'configuration/institutes',
    component: AdvancedListComponent,
    data: {
      model: 'Institutes',
      tableConfiguration: InstituteTableConfiguration,
      searchConfiguration: InstituteSearchConfiguration,
      category: 'Institutes',
      description: 'Settings.Lists.Institute.Description',
      longDescription: 'Settings.Lists.Institute.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        },
        resolve: {
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          serviceQueryParams: {
            $expand: 'registrationPeriods,instituteConfiguration,locales'
          },
          closeOnSubmit: true,
        },
        resolve: {
          data: AdvancedFormItemWithLocalesResolver
        }
      }
    ]
  },
    {
        path: 'current/documents/series',
        component: AdvancedListComponent,
        data: {
            model: 'DepartmentDocumentNumberSeries',
            tableConfiguration: DepartmentDocumentSeriesConfig,
            category: 'Sidebar.Departments',
            description: 'Documents.Lists.DepartmentDocumentNumberSeries.Description',
            longDescription: 'Documents.Lists.DepartmentDocumentNumberSeries.LongDescription'
        },
        children: [
            {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'new',
                    closeOnSubmit: true,
                    formConfig: DepartmentDocumentSeriesNew
                },
                resolve: {
                    department: ActiveDepartmentIDResolver
                }
            },
            {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'edit',
                    closeOnSubmit: true,
                    formConfig: DepartmentDocumentSeriesEdit
                },
                resolve: {
                    data: AdvancedFormItemResolver
                }
            }
        ]
    },
    {
        path: ':id',
        component: DepartmentsRootComponent,
        data: {
            title: 'Departments Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: DepartmentsDashboardComponent,
                data: {
                    title: 'Departments Dashboard'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: DepartmentsDashboardOverviewComponent,
                        data: {
                            title: 'Department\'s View'
                        }
                    },
                  {
                    path: 'registration',
                    component: DepartmentsDashboardRegistrationsComponent,
                    data: {
                      title: 'Theses Registrations Rules'
                    }
                  }
                ]

            },
            {
              path: 'edit',
              component: AdvancedFormRouterComponent,
              data: {
                action: 'edit',
                serviceQueryParams: {
                  $expand: 'locales'
                }
              },
              resolve: {
                data: AdvancedFormItemWithLocalesResolver
              }
            },
            {
                path: ':action',
                component: AdvancedFormRouterComponent
            }
        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class DepartmentsRoutingModule {
}

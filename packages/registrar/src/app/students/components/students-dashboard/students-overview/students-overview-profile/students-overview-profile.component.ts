import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-profile',
  templateUrl: './students-overview-profile.component.html',
  styleUrls: ['./students-overview-profile.component.scss']
})
export class StudentsOverviewProfileComponent implements OnInit, OnDestroy, OnChanges {

  public student;
  @Input() studentId: number;

  //  Value to indicate whether the message send form should be visible or not (This is passed to child component)
  public showMessageForm = false;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .expand('person($expand=gender), department, studyProgram, user')
        .getItem()
        .then((value) => { this.student = value; });
    }
  }

  ngOnInit() {
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        this.student =  await this._context.model('Students')
          .where('id').equal(this.studentId)
          .expand('person($expand=gender), department, studyProgram, user')
          .getItem();
      }
    });
  }

  /*
   *  This Functions toggles Message Send Form Visibility
   */
  enableMessages() {
    this.showMessageForm = !this.showMessageForm;
  }

  /*
   *  This functions is used to receive message sent status from child component
   */
  onsuccesfulSend(succesful: boolean) {
    //  Then toggles messge form visibility accordingly
    this.showMessageForm = !succesful;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}

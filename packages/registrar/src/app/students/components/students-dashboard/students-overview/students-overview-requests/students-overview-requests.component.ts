import { Component, Input, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-requests',
  templateUrl: './students-overview-requests.component.html',
  styleUrls: ['./students-overview-requests.component.scss']
})
export class StudentsOverviewRequestsComponent implements OnInit, OnDestroy {

  public lastRequests: any;
  @Input() studentId: number;
  private fragmentSubscription: any;
  private subscription: Subscription;

  constructor(private _context: AngularDataContext,
    private _activatedRoute: ActivatedRoute) { }

  async ngOnInit() {

    // do reload by using hidden fragment e.g. /lastRequests#reload
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.fetch().then(() => {});
      }
    });
    await this.fetch();
  }

  async fetch() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.lastRequests = await this._context.model('StudentRequestActions')
        .where('student').equal(this.studentId)
        .and('actionStatus/alternateName').equal('ActiveActionStatus')
        .orderByDescending('dateModified')
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';
import {RequestsTableComponent} from './components/requests-table/requests-table.component';
import {RequestsRootComponent} from './components/requests-root/requests-root.component';
import {RequestsEditComponent} from './components/requests-edit/requests-edit.component';
import {RequestsTableConfigurationResolver, RequestsTableSearchResolver} from './components/requests-table/requests-table-config.resolver';
import {AdvancedListComponent} from '../tables/components/advanced-list/advanced-list.component';
import {AdvancedTableConfigurationResolver} from '../tables/components/advanced-table/advanced-table-resolvers';
import {
  AdvancedFormItemResolver,
  AdvancedFormModalComponent,
  AdvancedFormModalData, AdvancedFormModelResolver,
  AdvancedFormResolver
} from '@universis/forms';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
  DocumentActionContainer
} from './components/request-document-action/preview/preview.component';
import {ExamPeriodParticipateActionContainer} from './components/exam-period-participate-action/preview/preview.component';
import {GraduationRequestActionContainer} from './components/graduation-request-action/preview/preview.component';


const routes: Routes = [
  {
    path: '',
    component: RequestsHomeComponent,
    data: {
      title: 'Requests'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: RequestsTableComponent,
        data: {
          title: 'Requests'
        },
        resolve: {
          tableConfiguration: RequestsTableConfigurationResolver,
          searchConfiguration: RequestsTableSearchResolver
        },
      }
    ]
  },
  {
    path: 'documents',
    component: AdvancedListComponent,
    data: {
      model: 'DocumentConfigurations',
      list: 'active',
      description: 'Requests.RequestTypes.RequestDocuments',
      longDescription: 'Requests.RequestTypes.RequestDocumentsDescription',
    },
    resolve: {
      tableConfiguration: AdvancedTableConfigurationResolver
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          description: null,
          action: 'new',
          closeOnSubmit: true
        },
        resolve: {
          formConfig: AdvancedFormResolver
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: 'types',
    component: AdvancedListComponent,
    data: {
      model: 'StudentRequestConfigurations',
      list: 'active',
      description: 'Requests.RequestTypes.Types',
      longDescription: 'Requests.RequestTypes.TypesDescription',
    },
    resolve: {
      tableConfiguration: AdvancedTableConfigurationResolver
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          description: null,
          action: 'new',
          closeOnSubmit: true
        },
        resolve: {
          formConfig: AdvancedFormResolver
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: ':id',
    component: RequestsRootComponent,
    data: {
      title: 'Request Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'edit'
      },
      {
        path: 'edit',
        component: RequestsEditComponent,
        data: {
          title: 'Request Edit'
        },
        children: [
          {
            path: 'edit/StudentRemoveActions/:id',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'StudentRemoveActions',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams : {
                $expand: 'object($expand=person,department)'
              }
            },
            resolve: {
              data: AdvancedFormItemResolver,
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: 'edit/StudentSuspendActions/:id',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'StudentSuspendActions',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams : {
                $expand: 'object($expand=person,department)'
              }
            },
            resolve: {
              data: AdvancedFormItemResolver,
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: 'edit/:model/:id',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: null,
              action: 'edit',
              closeOnSubmit: true
            },
            resolve: {
              model: AdvancedFormModelResolver,
              data: AdvancedFormItemResolver,
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: 'RequestMessageAction',
            component: AdvancedFormRouterComponent,
            outlet: 'as',
            data: {
              model: 'RequestMessageActions',
              action: 'preview',
              serviceParams : {
              }
            }
          },
          {
            path: 'RequestRemoveAction',
            component: AdvancedFormRouterComponent,
            outlet: 'as',
            data: {
              model: 'RequestRemoveActions',
              action: 'preview',
              serviceParams : {
              }
            }
          },
          {
            path: 'RequestSuspendAction',
            component: AdvancedFormRouterComponent,
            outlet: 'as',
            data: {
              model: 'RequestSuspendActions',
              action: 'preview',
              serviceParams : {
              }
            }
          },
          {
            path: 'RequestDocumentAction',
            component: DocumentActionContainer,
            outlet: 'as',
            data: {
              model: 'RequestDocumentActions',
              action: 'preview'
            }
          },
          {
            path: 'ExamPeriodParticipateAction',
            component: ExamPeriodParticipateActionContainer,
            outlet: 'as',
            data: {
              model: 'ExamPeriodParticipateActions'
            }
          },
          {
            path: 'GraduationRequestAction',
            component: GraduationRequestActionContainer,
            outlet: 'as',
            data: {
              model: 'GraduationRequestActions',
              action: 'preview',
              serviceParams : {
              }
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class RequestsRoutingModule {
}

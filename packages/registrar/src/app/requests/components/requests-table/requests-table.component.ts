import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import * as REQUESTS_LIST_CONFIG from './requests-table.config.list.json';
import { AdvancedTableComponent, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivatedTableService } from '../../../tables/tables.activated-table.service';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import {Observable, Subscription} from 'rxjs';
import {ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import {AppEventService} from '@universis/common';
import {AngularDataContext} from '@themost/angular';
import {ClientDataQueryable} from '@themost/client';
import {RequestActionComponent} from '../request-action/request-action.component';
import {DocumentSignActionComponent} from '../document-sign-action/document-sign-action.component';
import {ReportService} from '../../../reports-shared/services/report.service';
import {SignerService} from '../../../reports-shared/services/signer.service';

interface RequestDocumentActionSelectedRow {
  id: number;
  actionStatus: string;
  published?: boolean;
  datePublished?: any;
  signReport?: boolean;
  signed?: boolean;
  reportTemplate?: number;
  student?: number;
  documentNumber?: string;
  resultUrl?: string;
  parentDocumentSeries?: number;
}

@Component({
  selector: 'app-requests-table',
  templateUrl: './requests-table.component.html'
})
export class RequestsTableComponent implements OnInit, OnDestroy {

  public readonly config = REQUESTS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  private changeSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public showPublish = true;
  public showSign = true;

  private selectedItems = [];

  constructor(
    private _context: AngularDataContext,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _reports: ReportService,
    private _signer: SignerService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        // show or hide publish action
        this.showPublish = this.table.config.columns .findIndex( (column) => {
          return column.property === 'published';
        }) >= 0;
        // show or hide sign action
        this.showSign = this.table.config.columns .findIndex( (column) => {
          return column.property === 'signed';
        }) >= 0;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Requests.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      if  (this.table.dataTable == null) {
        return;
      }
      if (event && event.target && event.model === 'DocumentNumberSeriesItems') {
        this.table.fetchOne({
          result: event.target.id
        });
      }
      if (event && event.target && event.model === this.table.config.model) {
        this.table.fetchOne({
          id: event.target.id
        });
      }
    });

  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      // search for document attributes (if table has published column)
      const hasPublishedColumn = this.table.config.columns.find( (col) => {
        return col.property === 'published';
      });
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus'];
          if (hasPublishedColumn) {
            selectArguments.push('result/id as result',
                'object/reportTemplate/signReport as signReport',
                'result/signed as signed',
                'result/published as published',
                'result/datePublished as datePublished',
                'object/reportTemplate/id as reportTemplate',
              'student/id as student',
              'result/documentNumber as documentNumber',
              'result/url as resultUrl');
          }
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(-1)
              .skip(0)
              .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            if (hasPublishedColumn) {
              return <RequestDocumentActionSelectedRow>{
                id: item.id,
                actionStatus: item.actionStatus,
                result: item.result,
                signed: item.signed,
                signReport: item.signReport,
                published: item.published,
                datePublished: item.datePublished,
                reportTemplate: item.reportTemplate,
                documentNumber: item.documentNumber,
                student: item.student,
                resultUrl: item.resultUrl,
                parentDocumentSeries: item.parentDocumentSeries
              };
            } else {
              return {
                id: item.id,
                actionStatus: item.actionStatus
              };
            }

          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  private _executeSignActionOne(item, index) {
    return new Promise((resolve, reject) => {
      // set progress
      const total = this.selectedItems.length;
      this._context.model(this.table.config.model)
          .where('id').equal(item.id)
          .select('result')
          .expand('result')
          .getItem().then((res) => {
        if (res == null) {
          return reject(new Error('The specified item cannot be found'));
        }
        // set component item which is the current document
        if (res.result == null) {
          return reject(new Error('The specified document cannot be found'));
        }
        const component = <DocumentSignActionComponent>this._modalService.modalRef.content;
        const document = res.result;
        setTimeout(() => {
          // sign document
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          component.sign(document).then(() => {
            // log success
            return this.table.fetchOne({
              id: item.id
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
          }).catch((err) => {
            // log error
            return resolve({
              success: 0,
              error: err
            });
          });
        }, 750);
      }).catch((err) => {
        // log error
        console.log(err);
        return resolve({
          success: 0,
          error: err
        });
      });
    });
  }

  executeSignAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let i = 0; i < this.selectedItems.length; i++) {
          const item = this.selectedItems[i];
          const res: any = await this._executeSignActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executePublishAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      const datePublished = new Date();
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.result,
          published: true,
          datePublished: datePublished
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('DocumentNumberSeriesItems').save(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  /***
   * Accepts the selected requests
   */
  async acceptAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.AcceptAction.Title',
          description: 'Requests.Edit.AcceptAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Publishes the selected requests
   */
  async publishAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return (item.actionStatus === 'CompletedActionStatus') &&
            (!!item.published === false) &&
            ((!!item.signReport === false) || (!!item.signReport === true && item.signed === true));
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.PublishAction.Title',
          modalIcon: 'far fa-newspaper mt-0',
          description: 'Requests.Edit.PublishAction.Description',
          refresh: this.refreshAction,
          execute: this.executePublishAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Rejects the selected requests
   */
  async rejectAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.RejectAction.Title',
          description: 'Requests.Edit.RejectAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async signAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'CompletedActionStatus' &&
            item.result != null &&
            item.signed === false;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(DocumentSignActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          item: {
          },
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.SignAction.Title',
          description: 'Requests.Edit.SignAction.Description',
          refresh: this.refreshAction,
          execute: this.executeSignAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  private _executeReissueActionOne(item: RequestDocumentActionSelectedRow, index: number) {

    const total = this.selectedItems.length;
    return new Promise((resolve, reject) => {
      this._reports.printReport(item.reportTemplate, {
        ID: item.student,
        REPORT_USE_DOCUMENT_NUMBER: false,
        REPORT_DOCUMENT_SERIES: item.parentDocumentSeries,
        REPORT_DOCUMENT_NUMBER: item.documentNumber
      }).then((blob) => {
        // replace document
        const updateItem = {
          url: item.resultUrl,
          published: false,
          datePublished: null,
          signed: false
        };
        return this._signer.replaceDocument(updateItem, blob).then(() => {
          setTimeout(() => {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            return this.table.fetchOne({
              id: item.id
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
          }, 500);
        });
      }).catch((err) => {
        return resolve({
          success: 0,
          error: err
        });
      });
    });

  }

  executeReissueAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let i = 0; i < this.selectedItems.length; i++) {
          const item = this.selectedItems[i];
          const res: any = await this._executeReissueActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Publishes the selected requests
   */
  async reissueAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: RequestDocumentActionSelectedRow) => {
        return ((item.actionStatus === 'CompletedActionStatus') &&
          (item.published === false) &&
          (item.resultUrl != null)
        );
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.ReissueAction.Title',
          modalIcon: 'far fa-print mt-0',
          description: 'Requests.Edit.ReissueAction.Description',
          refresh: this.refreshAction,
          execute: this.executeReissueAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}

import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActiveDepartmentService} from "../../../registrar-shared/services/activeDepartmentService.service";

@Component({
  selector: 'app-dashboard-pending-student-requests',
  templateUrl: './dashboard-pending-student-requests.component.html',
  styleUrls: ['./dashboard-pending-student-requests.component.scss']
})
export class DashboardPendingStudentRequestsComponent implements OnInit {

  public lastRequests: any;

  constructor(private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService ) { }

  async ngOnInit() {
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();

    this.lastRequests = await this._context.model('StudentRequestActions')
      .where('actionStatus/alternateName ').equal('ActiveActionStatus')
      .and('student/department').equal(activeDepartment.id)
      .orderByDescending('dateModified')
      .take(3)
      .getList();
  }

}

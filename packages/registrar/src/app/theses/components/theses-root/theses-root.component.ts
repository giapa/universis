import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep, template } from 'lodash';
import * as THESES_LIST_CONFIG from '../theses-table/theses-table.config.json';
import { TableConfiguration } from '../../../tables/components/advanced-table/advanced-table.interfaces';
import { Subscription } from 'rxjs';
import { UserActivityService } from '@universis/common';

@Component({
  selector: 'app-theses-root',
  templateUrl: './theses-root.component.html',
})
export class ThesesRootComponent implements OnInit, OnDestroy {
  public model: any;
  public isCreate = false;
  public tabs: any[];
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _userActivityService: UserActivityService) { }

  async ngOnInit() {

    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Theses')
        .where('id').equal(params.id)
        .expand('instructor,type,status,students($expand=student($expand=department,person))')
        .getItem();
      if (this.model) {
        this._userActivityService.setItem({
          category: this._translateService.instant('Theses.Title'),
          description: this._translateService.instant(this.model.students[0].student.person.familyName +
                       ' ' + this.model.students[0].student.person.givenName),
          url: '/theses/' + params.id + '/dashboard',
          dateCreated: new Date
        });

        // @ts-ignore
        this.config = cloneDeep(THESES_LIST_CONFIG as TableConfiguration);

        if (this.config.columns && this.model) {
          // get actions from config file
          this.actions = this.config.columns.filter(x => {
            return x.actions;
          })
            // map actions
            .map(x => x.actions)
            // get list items
            .reduce((a, b) => b, 0);

          // filter actions with student permissions
          this.allowedActions = this.actions.filter(x => {
            if (x.role) {
              if (x.role === 'action') {
                return x;
              }
            }
          });

          this.edit = this.actions.find(x => {
            if (x.role === 'edit') {
              x.href = template(x.href)(this.model);
              return x;
            }
          });

          this.actions = this.allowedActions;
          this.actions.forEach(action => {
            action.href = template(action.href)(this.model);
          });
        }

      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

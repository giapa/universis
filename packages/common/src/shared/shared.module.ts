import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {LocalizedDatePipe} from './pipes/localized-date.pipe';
import {SemesterPipe} from './pipes/semester.pipe';
import {MsgboxComponent} from './components/msgbox/msgbox.component';
import {DialogComponent} from './components/modal/dialog.component';
import {SpinnerComponent} from './components/modal/spinner.component';
import {APP_CONFIGURATION, ApplicationConfiguration, ConfigurationService} from './services/configuration.service';
import {LoadingService} from './services/loading.service';
import {HttpClientModule} from '@angular/common/http';
import {ToastComponent} from './components/modal/toast.component';
import {ToastService} from './services/toast.service';
import {ModalService} from './services/modal.service';
import {GradePipe, GradeScaleService} from './services/grade-scale.service';
import {NgVarDirective} from './directives/ngvar.directive';
import {UserStorageService} from './services/user-storage';
import { UserActivityService } from './services/user-activity/user-activity.service';
import { SessionUserActivityService } from './services/session-user-activity/session-user-activity.service';
import {
  PersistentUserActivityService
} from './services/persistent-user-activity/persistent-user-activity.service';
import {DiagnosticsService} from './services/diagnostics.service';
import { RequestTypesService } from './services/request-types/request-types.service';
import {AppSidebarService, SIDEBAR_LOCATIONS} from './services/app-sidebar.service';
import {GUEST_SIDEBAR_LOCATIONS} from './services/app-guest-sidebar.service';
import {LocalUserStorageService} from './services/browser-storage.service';
import {SessionUserStorageService} from './services/browser-storage.service';
import {FallbackUserStorageService} from './services/fallback-user-storage.service';
import {AppEventService} from './services/app-event.service';
import {TemplatePipe} from './pipes/template.pipe';
import {LocalizedAttributesPipe} from './pipes/localized-attributes.pipe'
import {XmasSpinnerComponent} from './components/modal/xmas-spinner.component';
import {XmasLoadingService} from './services/xmas-loading.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    TranslateModule,
  ],
  declarations: [
    LocalizedDatePipe,
    GradePipe,
    SemesterPipe,
    MsgboxComponent,
    DialogComponent,
    SpinnerComponent,
    ToastComponent,
    NgVarDirective,
    TemplatePipe,
    LocalizedAttributesPipe,
    XmasSpinnerComponent
  ],
  entryComponents: [
    DialogComponent,
    SpinnerComponent,
    ToastComponent,
    XmasSpinnerComponent
  ],
  providers: [
    {
      provide: APP_CONFIGURATION,
      useValue: <ApplicationConfiguration>{
        settings: {
          remote: {
            server: '/'
          },
          i18n: {
            locales: [ 'en' ],
            defaultLocale: 'en'
          }
        }
      }
    },
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: []
    },
    {
      provide: GUEST_SIDEBAR_LOCATIONS,
      useValue: []
    }
  ],
  exports: [
    LocalizedDatePipe,
    GradePipe,
    SemesterPipe,
    TemplatePipe,
    MsgboxComponent,
    DialogComponent,
    SpinnerComponent,
    ToastComponent,
    NgVarDirective,
    LocalizedAttributesPipe],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        ConfigurationService,
        ModalService,
        ToastService,
        LoadingService,
        XmasLoadingService,
        GradeScaleService,
        UserStorageService,
        UserActivityService,
        SessionUserActivityService,
        PersistentUserActivityService,
        DiagnosticsService,
        RequestTypesService,
        AppSidebarService,
        LocalUserStorageService,
        SessionUserStorageService,
        FallbackUserStorageService,
        AppEventService
      ]
    };
  }
}

import { TestBed, async } from '@angular/core/testing';
import {Component} from '@angular/core';
import {NgVarDirective} from './ngvar.directive';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
    selector: 'universis-test-root',
    template: `<div [ngVar]="'Hello World!'" #message="ngVar">
        <span>{{message.value}}</span>
        <div id="divElement" [ngVar]="'Bonjour le monde!'" #innerMessage="ngVar">
            <span>{{innerMessage.value}}</span>
        </div>
        <button (click)="message.value = 'Hallo Welt!';">Say Hello</button>
    </div>`
})
// ts-ignore
export class TestAppComponent {
    title = 'registrar';
}


describe('VarDirective', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                FormsModule
            ],
            declarations: [
                TestAppComponent,
                NgVarDirective
            ],
        }).compileComponents();
    }));

    it('should render value from var', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('span').textContent).toContain('Hello World!');
    });

    it('should render another value from var', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('#divElement>span').textContent).toContain('Bonjour le monde!');
    });

    it('should render change var value', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        const button = <HTMLButtonElement>compiled.querySelector('button');
        button.click();
        fixture.detectChanges();
        expect(compiled.querySelector('span').textContent).toContain('Hallo Welt!');

    });

});

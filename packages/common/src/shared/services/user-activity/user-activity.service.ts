import { Injectable } from '@angular/core';
import { uniqBy, slice } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 *
 * @interface UserActivityEntry
 *
 * UserActivityEntry is a single entry at the user activity list
 *
 */
export declare interface UserActivityEntry {
  category: string;
  description: string;
  url: string;
  dateCreated: Date;
}


/**
 * UserActivityService
 *
 * Handles the user activity tracking.
 *
 */

@Injectable()
export class UserActivityService {

  protected userActivityEntries$: BehaviorSubject<Array<UserActivityEntry>>;
  protected maxItems = 10;
  protected list: Array<UserActivityEntry> = [];


  constructor() {
    this.userActivityEntries$ = new BehaviorSubject([]);
  }

  /**
   *
   * Adds a single item at the userActivity service.
   *
   * @param {UserActivityEntry} entry The entry to be written
   *
   */
  public async setItem(entry: UserActivityEntry): Promise<void> {

    if (!entry) {
      throw new Error('Entry may not be null');
    }

    if (!entry.url) {
      throw new Error('Entry URL may not be empty');
    }

    const snapshot = [...this.list];
    snapshot.unshift(entry);

    const filteredList = uniqBy(snapshot, ({ url }) => url);
    const limitedList = slice(filteredList, 0, this.maxItems);
    this.list = limitedList;

    if (this.userActivityEntries$) {
      this.userActivityEntries$.next(limitedList);
    }
  }

  /**
   *
   * Get the list of the user activity.
   *
   * @returns {Array<UserActivityEntry>} The list of the entries
   *
   */
  public async getItems(): Promise<Array<UserActivityEntry>> {
    return this.list;
  }

  /**
   *
   * Get the list of the user activity as observable
   *
   */
  public async getItemsAsObservable(): Promise<Observable<Array<UserActivityEntry>>> {
    return this.userActivityEntries$.asObservable();
  }
}

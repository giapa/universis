import {Component, ElementRef} from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
})
export class ColorsComponent {
  public getRGBCode (cssClass: string) {
    return $(cssClass).css('background-color');
  }

  public getHexCode(cssClass: string) {
    const color = this.getRGBCode(cssClass);
    const rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return '#' + this.hex(rgb[1]) + this.hex(rgb[2]) + this.hex(rgb[3]);
  }
  public hex(x) {
    // tslint:disable-next-line:radix
    return ('0' + parseInt(x).toString(16)).slice(-2);
  }
}

const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');
// read common index.html
fs.readFile(path.resolve(__dirname, '../public/packages/common/index.html'), 'utf8', (err, data) => {
    if (err) {
        console.log(err);
        return process.exit(-1);
    }
    const $ = cheerio.load(data);
    // set relative path for readme links
    $('a[href="packages/registrar/"]').attr('href', '../../packages/registrar/');
    $('a[href="packages/common/"]').attr('href', '../../packages/common/');
    $('a[href="packages/theme/"]').attr('href', '../../packages/theme/');
    // write common index.html
    fs.writeFile(path.resolve(__dirname, '../public/packages/common/index.html'), $.html(), 'utf8', (err) => {
        if (err) {
            console.log(err);
            return process.exit(-1);
        }
        return process.exit(0);
    });
});
